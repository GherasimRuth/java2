/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication_penta;

import java.awt.Dimension;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 *
 * @author LAURA
 */
public class View extends javax.swing.JFrame{

     private JTextField firstNumber;
     private JTextField secondNumber;
     private JButton btnVerify;
     private JLabel lblVerify;
     private JLabel lblFirstNumber;
     private JLabel lblSecondNumber;
     
     
     
     public View()
     {
        this.setPreferredSize(new Dimension(800, 600));
        this.setLayout(null);
        
         // set txtFirstName
        firstNumber = new JTextField();
        firstNumber.setBounds(30, 50, 200, 50);
        this.add(firstNumber);
        
        // set txtFirstName
        secondNumber = new JTextField();
        secondNumber.setBounds(250, 50, 200, 50);
        this.add(secondNumber);
        
        // set btnVerify
        btnVerify = new JButton();
        btnVerify.setText("Verify");
        btnVerify.setBounds(30, 100, 200, 50);
        this.add(btnVerify);
        
        
        // set lblFirstName
        lblVerify = new JLabel();
        lblVerify.setText("");
        lblVerify.setBounds(30, 150, 200, 50);
        this.add(lblVerify);
        
        // set lblFirstNumber
        lblFirstNumber = new JLabel();
        lblFirstNumber.setText("Primul numar");
        lblFirstNumber.setBounds(30, 1, 200, 50);
        this.add(lblFirstNumber);
        
         // set lblSecondNumber
        lblSecondNumber = new JLabel();
        lblSecondNumber.setText("Al 2-lea numar");
        lblSecondNumber.setBounds(250, 1, 200, 50);
        this.add(lblSecondNumber);
      
        
        
        this.pack();
        this.setVisible(true);
         
     }
      public String getFirstNumber() {
        return firstNumber.getText();
    }
      public String getSecondNumber() {
        return secondNumber.getText();
    }
      public JButton btnVerify() {
        return btnVerify;
    }
      public void setLabel(String s)
      {
          lblVerify.setText(s);
      }
     
  
}
